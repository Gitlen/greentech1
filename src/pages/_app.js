import '@/styles/globals.css'
import Navbar from '@/components/Navbar/Navbar'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { AuthProvider } from '@/auth';
import '@fontsource/ubuntu/300.css';
import '@fontsource/ubuntu/400.css';
import '@fontsource/ubuntu/500.css';
import '@fontsource/ubuntu/700.css';



export default function App({ Component, pageProps }) {


  return (
    <AuthProvider>
      <Navbar />
      <Component {...pageProps} />
    </AuthProvider>
  )
}
