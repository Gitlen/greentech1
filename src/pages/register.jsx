import RegisterMain from "@/components/registerPages/RegisterMain";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { AnimatePresence } from "framer-motion";
import PageOne from "@/components/registerPages/WelcomePage";
import PageTwo from "@/components/registerPages/PageTwo";
import PageThree from "@/components/registerPages/PageThree";
import PageFour from "@/components/registerPages/PageFour";

export default function Register() {
    const router = useRouter()
    const [currentPage, setCurrentPage] = useState(0);

    return (
        <main className={"flex flex-col h-screen mx-auto"}>
            <AnimatePresence>
                {currentPage === 0 ? (
                    <PageTwo
                        prevPage={() => setCurrentPage(router.back())}
                        nextPage={() => setCurrentPage(currentPage + 1)}
                    />
                ) : currentPage === 1 ? (
                    <PageThree
                        prevPage={() => setCurrentPage(currentPage - 1)}
                        nextPage={() => setCurrentPage(currentPage + 1)}
                    />
                ) : currentPage === 2 ? (
                    <PageFour
                        prevPage={() => setCurrentPage(currentPage - 1)}
                        nextPage={() => setCurrentPage(currentPage + 1)}
                    />  
                ) : (
                    <RegisterMain goBack={() => setCurrentPage(currentPage - 1)} />
                )}
            </AnimatePresence>
        </main>
    );
}