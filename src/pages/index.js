import { useRouter } from "next/router";
import { AnimatePresence } from "framer-motion";
import WelcomePage from "@/components/registerPages/WelcomePage";

export default function Home() {
    const router = useRouter()

    return (
        <main className={"flex flex-col h-screen mx-auto"}>
            <AnimatePresence>
                    <WelcomePage goBack={() => router.back()} nextPage={() => setCurrentPage(currentPage + 1)} />
            </AnimatePresence>
        </main>
    );
}