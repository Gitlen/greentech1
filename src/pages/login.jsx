import SignIn from "@/components/auth/SignIn";
import { useRouter } from "next/router";

export default function Login() {
    const router = useRouter()
    return (
        <main className="flex flex-col h-screen mx-auto">
            {/* This is the "Go Back" btn */}
            <img onClick={() => { router.back() }} src="/backIcon.svg" className="absolute top-3 left-3" />
            {/* This is the orange top section with the white text */}
            <section className="bg-amber-500 w-screen h-1/4 rounded-b-[50px] flex flex-col
            items-center justify-end pb-5 space-y-1">
                <h1 className="text-white font-semibold text-4xl font-ubuntu font-bold">Logga in</h1>
                <p className="text-white leading-4 tracking-wider font-ubuntu font-medium text-md">Välkommen tillbaka!</p>
            </section>
            {/* This is the login section with all the inputs and everything */}
            <section className="w-4/5 mx-auto h-3/5">
                <SignIn />
            </section>

        </main>
    )
}