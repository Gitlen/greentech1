import { useState } from "react";
import { auth } from "../firebase";
import { sendPasswordResetEmail } from "firebase/auth";
import { useRouter } from "next/router";

export default function ForgotPassword() {
    const router = useRouter();
    const [email, setEmail] = useState("");
    const [modalState, setModalState] = useState(true)

    const handleForgotPassword = () => {
        sendPasswordResetEmail(auth, email)
            .then(() => {
                setModalState(true)
                console.log("Password reset email sent");
            })
            .catch((error) => {
                console.error(error);
            });

    }

    return (
        <main className="flex flex-col h-screen mx-auto">
            {modalState &&
                <section className="absolute flex flex-col items-center space-y-5 rounded-md w-4/5 py-6 bg-emerald-500 top-2/4 left-2/4 -translate-y-2/4 -translate-x-2/4 shadow-[rgba(0,_0,_0,_0.25)_0px_25px_50px_-12px]">
                    <h1 className="font-ubuntu text-gray-100 w-5/6 text-center mx-auto">Din länk för återställan av lösenord är skickad! Glöm inte att kolla skräpposten!</h1>
                    <button onClick={() => setModalState(false)} className="w-5/6 bg-white text-emerald-500 py-2 text-xl rounded-lg">
                        OK
                    </button>
                </section>
            }

            {/* This is the "Go Back" btn */}
            <img onClick={() => { router.back() }} src="/backIcon.svg" className="absolute top-3 left-3" />
            {/* This is the orange top section with the white text */}
            <section className="bg-emerald-500 w-screen h-1/4 rounded-b-[50px] flex flex-col
            items-center justify-end pb-5 space-y-1">
                <h1 className="text-white font-semibold text-4xl font-ubuntu font-bold">Glömt lösenord</h1>
                <p className="text-white leading-4 tracking-wider font-ubuntu font-medium text-sm max-w-[15rem] text-center">En länk för återställan kommer att skickas till din email.</p>
            </section>
            {/* This is the login section with all the inputs and everything */}
            <section className="w-4/5 mx-auto h-3/5">
                <form onSubmit={() => handleForgotPassword(email)}>
                    <div className="w-full relative mt-16">
                        <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="email" value={email}
                            onChange={(e) => { setEmail(e.target.value) }} />
                        <p className="text-gray-500 text-xs font-ubuntu font-medium absolute -top-2 left-3 bg-white px-2">Email Address</p>
                    </div>
                    <button className="border-2 border-emerald-500 text-emerald-500 w-full font-normal font-ubuntu px-6 py-2 rounded-3xl mt-14" type="sumbit">Skicka länk</button>
                </form>
            </section>
        </main>
    )
}
