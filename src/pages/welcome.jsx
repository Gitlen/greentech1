import { useContext, useEffect, useState } from "react"
import { AuthContext } from "@/auth"
import { signOut } from "firebase/auth"
import { auth } from "../firebase"
import { doc, getDoc } from "firebase/firestore";
import { db } from "../firebase";

//THIS IS JUST A PAGE TO TEST IF WE CAN RETRIEVE DATA FROM AUTH USER AND FIRESTORE DATABASE.
//NO NEED TO TOUCH THIS PAGE IF NOT TOLD OTHERWISE. THIS PAGE WILL PROBABLY BE REMOVED BEFORE
//DEPLOYMENT.

export default function Welcome() {
    const [schoolName, setSchoolName] = useState("");
    const [className, setClassName] = useState("");
    const { currentUser } = useContext(AuthContext)
    console.log(currentUser)


    useEffect(() => {
        if (currentUser) {
            const docRef = doc(db, "users", currentUser.uid);
            getDoc(docRef)
                .then((docSnap) => {
                    if (docSnap.exists()) {
                        setClassName(docSnap.data().className)
                        setSchoolName(docSnap.data().schoolName);
                    }
                    else {
                        console.error("User has not specified school or class")
                    }
                })
                .catch((error) => {
                    console.log("Error getting document:", error);
                });
        }
    }, [currentUser]);





    return (
        <div className="flex flex-col justify-center items-center h-screen">
            {currentUser &&
                <div>
                    <h1>{currentUser.displayName}</h1>
                    <h1>{schoolName && schoolName}{(schoolName && className) && ","} {className && className}</h1>
                </div>
            }
            <button
                className="bg-red-400 text-white text-6xl font-ubuntu px-6 py-3 rounded-xl"
                onClick={() => signOut(auth)}>Sign out!</button>
        </div>
    )
}