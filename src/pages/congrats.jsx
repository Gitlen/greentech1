import CongratsPage from "@/components/registerPages/registrationVerification/CongratsPage";

export default function Congrats(){
  return (
    <div className="w-screen h-screen">
  <CongratsPage />
  </div>
  )
}