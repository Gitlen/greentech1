import React, { useEffect, useState } from "react"
import { auth } from "./firebase"
import { onAuthStateChanged } from "firebase/auth"
import Router from "next/router";

export const AuthContext = React.createContext();

export const AuthProvider = ({ children }) => {
    const [currentUser, setCurrentUser] = useState(null);

    useEffect(() => {
        onAuthStateChanged(auth, (user) => {
            if (user) {
                setCurrentUser(user);
                if (user.displayName) {
                    console.log("User is logged in as " + user.displayName)
                } else {
                    console.error("User has not specified name" + user.displayName)
                }

                if (Router.pathname === "/login")
                    Router.push("/welcome")
            } else {
                setCurrentUser(null);
                console.log("user is logged out!")
                if (Router.pathname !== "/login" && Router.pathname !== "/register" && Router.pathname !== "/forgot-password" && Router.pathname !== "/congrats") {
                    Router.push("/")
                }


            }
        })
    }, [])


    return (
        <AuthContext.Provider
            value={{
                currentUser
            }}>
            {children}
        </AuthContext.Provider>
    )

}