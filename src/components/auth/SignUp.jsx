import { useState } from "react";
import Link from "next/link";
import { auth, db } from "../../firebase";
import { createUserWithEmailAndPassword, sendEmailVerification, updateProfile } from "firebase/auth";
import { doc, setDoc } from "firebase/firestore";
//import kommuner from "../../kommuner.json"
import { motion as m } from "framer-motion"
import Router from "next/router";
import {AiOutlineLoading} from "react-icons/ai"


export default function SignUp() {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [school, setSchool] = useState("");
    const [hiddenPw, setHiddenPw] = useState(false);
    const [loaded, setLoaded] = useState(true)

    const signUp = async (e) => {
        setLoaded(false);
        e.preventDefault();
        try {
            const { user } = await createUserWithEmailAndPassword(auth, email, password);
            updateProfile(user, {
                displayName: username
            })
            console.log('User registered successfully!');
            Router.push("/congrats");
            console.log("pushed user");
            setLoaded(false)

            try {
                await setDoc(doc(db, "users", user.uid), {
                schoolName: school,
            });
            } catch(error){
                console.log("Failed to send schoolname to databse.", error)
            }

            await sendEmailVerification(user);
        } catch (error) {
            console.error('Error registering user:', error.message);
        }
    }


    return (
        <div className="w-full mt-16 h-full">
            {!loaded && <AiOutlineLoading 
            size={"5rem"}
            color="#134E4A"
            className="loginSpin absolute left-2/4 top-2/4 z-[1000] opacity-70" />}
            <form onSubmit={signUp} className="w-full h-full flex flex-col justify-between">
                <div className="w-full space-y-6">
                    <m.div
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .3 }}
                        className="w-full relative">
                        <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="text" value={school}
                            onChange={(e) => { setSchool(e.target.value) }} />
                        <p className="text-gray-500 text-xs font-ubuntu font-medium absolute -top-2 left-3 bg-white px-2">Skola</p>
                    </m.div>
                    <m.div
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .4 }}
                        className="w-full relative">
                        <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="text" value={username}
                            onChange={(e) => { setUsername(e.target.value) }} />
                        <p className="text-gray-500 text-xs font-ubuntu font-medium absolute -top-2 left-3 bg-white px-2">Namn</p>
                    </m.div>
                    <m.div
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .5 }}
                        className="w-full relative">
                        <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="email" value={email}
                            onChange={(e) => { setEmail(e.target.value) }} />
                        <p className="text-gray-500 text-xs font-ubuntu font-medium absolute -top-2 left-3 bg-white px-2">Email Address</p>
                    </m.div>
                    <m.div
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .6 }}
                        className="w-full relative">
                        {hiddenPw ?
                            <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="text" value={password}
                                onChange={(e) => { setPassword(e.target.value) }} />
                            :
                            <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="password" value={password}
                                onChange={(e) => { setPassword(e.target.value) }} />
                        }
                        <p className="text-gray-500 text-xs absolute -top-2 left-3 bg-white px-2 font-ubuntu font-medium">Lösenord</p>
                        <img onClick={() => { setHiddenPw(!hiddenPw) }} className="absolute top-2/4 -translate-y-[50%] right-2 w-6" src={!hiddenPw ? "./hidden.png" : "/notHidden.png"} />
                    </m.div>
                </div>
                <m.div
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{ type: "spring", delay: .7 }}
                    className="mb-10">
                    <button className="bg-teal-900 w-full text-white font-normal font-ubuntu px-6 py-2 rounded-3xl" type="sumbit">Registrera</button>
                    <Link href={"/"}>
                        <p className="font-ubuntu max-w-[14rem] font-normal text-xs text-center mt-1 mx-auto">Genom att skapa ett konto går du med på våra <span className=" underline">villkor och interigetpolicy.</span></p>
                    </Link>
                </m.div>
            </form>
        </div>
    )
}