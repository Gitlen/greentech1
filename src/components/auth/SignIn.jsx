import { useState, useEffect } from "react";
import { auth } from "../../firebase";
import { signInWithEmailAndPassword, signInWithGoogle, signInWithPopup, signOut } from "firebase/auth";
import Link from "next/link"

//TODO: Google, Facebook, Twitter LOGIN. And register page.

export default function SignIn() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [hiddenPw, setHiddenPw] = useState(false)

    const signIn = (e) => {
        e.preventDefault();
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredentials) => { console.log("userCreds", userCredentials) })
            .catch((error) => console.log("error", error));
    }

    //UNDER DEVELOPMENT
    const signInWithGoogle = (e) => {
        e.preventDefault();
        signInWithPopup(auth)
    }

    return (
        <div className="w-full mt-16 h-full">
            <form onSubmit={signIn} className="w-full h-full flex flex-col justify-between">
                <div className="w-full space-y-6">
                    <div className="w-full relative">
                        <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="email" value={email}
                            onChange={(e) => { setEmail(e.target.value) }} />
                        <p className="text-gray-500 text-xs font-ubuntu font-medium absolute -top-2 left-3 bg-white px-2">Email Address</p>
                    </div>
                    <div className="w-full relative">
                        {hiddenPw ?
                            <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="text" value={password}
                                onChange={(e) => { setPassword(e.target.value) }} />
                            :
                            <input className="w-full p-2 border-[1.5px] rounded-xl border-gray-500" type="password" value={password}
                                onChange={(e) => { setPassword(e.target.value) }} />
                        }
                        <p className="text-gray-500 text-xs absolute -top-2 left-3 bg-white px-2 font-ubuntu font-medium">Lösenord</p>
                        <img onClick={() => { setHiddenPw(!hiddenPw) }} className="absolute top-2/4 -translate-y-[40%] right-2" src="/hidePassword.svg" />
                        <Link href={"/forgot-password"}>
                            <p className="font-ubuntu text-xs absolute -bottom-5 right-1 text-amber-600">Glömt lösenord</p>
                        </Link>
                    </div>
                </div>
                <div>
                    <button className="bg-amber-500 w-full text-white font-normal font-ubuntu px-6 py-2 rounded-3xl" type="sumbit">Logga in</button>
                    <Link href={"/register"}>
                        <p className="font-ubuntu font-normal text-sm text-center mt-1">Har du inget konto? <span className="text-amber-500">Skapa konto</span></p>
                    </Link>
                    <div>
                        <div className="w-full h-[1px] bg-black relative my-12">
                            <p className="bg-white w-28 text-center font-ubuntu font-normal text-xs absolute top-2/4 left-2/4 -translate-x-2/4 -translate-y-2/4">eller logga in med</p>
                        </div>
                        <div className="w-full flex justify-between px-5">
                            <img src="/Icon_facebook.svg" />
                            <img src="/Icon_twitter.svg" />
                            <img src="/Icon_google.svg" />
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}