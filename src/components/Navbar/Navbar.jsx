import { AiOutlineDollarCircle } from "react-icons/ai"
import { HiOutlineHome } from "react-icons/hi"
import { BiMap } from "react-icons/bi"
import NavbarItem from "./NavbarItem"
import { useContext } from "react"
import { useRouter } from "next/router"
import { AuthContext } from "@/auth"


export default function Navbar() {
    const { currentUser } = useContext(AuthContext)
    const router = useRouter()

    // Check if the current route is one of the routes where the navbar should be hidden
    const hideNavbarRoutes = ["/", "/login", "/register", "/forgot-password", "/congrats"]
    const hideNavbar = hideNavbarRoutes.includes(router.pathname)

    const scale = "1.7rem"
    return (
        <>
            {!hideNavbar && (
                <nav className="fixed bottom-2 left-2/4 -translate-x-2/4 w-11/12 h-20 flex bg-teal-900 rounded-3xl opacity-80">
                    <NavbarItem href={"/karta"} icon={<BiMap color="white" size={scale} />} title={"Karta"} />
                    <NavbarItem href={"/klassen"} icon={<HiOutlineHome color="white" size={scale} />} title={"Klassen"} />
                    <NavbarItem href={"/kashing"} icon={<AiOutlineDollarCircle color="white" size={scale} />} title={"Kashing"} />

                </nav>
            )}
        </>

    )
}