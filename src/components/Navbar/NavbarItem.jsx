import Link from "next/link";
import { useState } from "react";

export default function NavbarItem({ icon, title, href }) {
    const [clicked, setClicked] = useState(false);
    return (
        <Link
            href={href}
            className="flex h-full w-1/3 flex-col justify-center items-center">
            {icon}
            <h1 className="mt-0 text-center tracking-tight leading-5 text-white">{title}</h1>
        </Link>
    )
}