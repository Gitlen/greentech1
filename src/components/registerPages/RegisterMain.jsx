import SignUp from "../auth/SignUp"
import { motion as m } from "framer-motion"

export default function RegisterMain({ goBack }) {
    return (
        <main className="flex flex-col h-screen mx-auto">
            {/* This is the "Go Back" btn */}
            <m.img
                initial={{ scale: 0 }}
                animate={{ scale: 1 }}
                transition={{ type: "spring", delay: .4 }}
                onClick={goBack} src="/backIcon.svg" className="absolute top-3 left-3" />
            {/* This is the orange top section with the white text */}
            <section className="bg-teal-900 w-screen h-1/4 rounded-b-[50px] flex flex-col
            items-center justify-end pb-5 space-y-1">
                <m.h1
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{ type: "spring", delay: .1 }}
                    className="text-white text-3xl font-ubuntu font-bold">Registrera din klass</m.h1>
                <m.p
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{ type: "spring", delay: .2 }}
                    className="text-white leading-4 tracking-wider font-ubuntu font-normal text-md">För klassen & naturen!</m.p>
            </section>
            {/* This is the login section with all the inputs and everything */}
            <section className="w-4/5 mx-auto h-3/5">
                <SignUp />
            </section>

        </main>
    )
}