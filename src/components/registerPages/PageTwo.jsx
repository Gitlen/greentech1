import { motion as m } from "framer-motion";
import Link from "next/link";

export default function PageTwo({ prevPage, nextPage }) {
    return (
        <>
            <section className="bg-orange-400 w-screen h-4/5 rounded-b-[50px] flex flex-col
            items-center justify-center pb-5 space-y-1">
                <m.h1
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{ type: "spring", delay: .3 }}
                    className="font-ubuntu text-3xl text-white">SOMETHING</m.h1>
            </section>
            <section className="h-full text-center flex flex-col justify-between pb-10 tall:pb-28 pt-10">
                <div className="space-y-6 flex flex-col items-center">
                    <m.h1
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .1 }}
                        className="text-3xl font-ubuntu">Registrera din klass</m.h1>
                    <m.p
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .2 }}
                        className="text-md max-w-[14rem] font-ubuntu">
                        Och gör en insats till naturen och stadens invånare på köpet
                    </m.p>
                </div>
                <div className="flex flex-col space-y-6 w-3/4 mx-auto">
                    <div className="flex justify-between">
                        <m.button
                            initial={{ scale: 0 }}
                            animate={{ scale: 1 }}
                            transition={{ type: "spring", delay: .3 }}
                            onClick={prevPage} className="border-teal-900 border-2 w-28 text-teal-900 font-normal font-ubuntu px-6 py-2 rounded-3xl">
                            Backa
                        </m.button>
                        <m.button
                            initial={{ scale: 0 }}
                            animate={{ scale: 1 }}
                            transition={{ type: "spring", delay: .4 }}
                            onClick={nextPage} className="bg-orange-400 w-28 text-black font-normal font-ubuntu px-6 py-2 rounded-3xl">
                            Nästa
                        </m.button>
                    </div>
                    <Link href={"/login"}>
                        <m.button
                            initial={{ scale: 0 }}
                            animate={{ scale: 1 }}
                            transition={{ type: "spring", delay: .5 }}
                            className="border-teal-900 border-2 w-full text-teal-900 font-normal font-ubuntu px-6 py-2 rounded-3xl">
                            Logga in
                        </m.button>
                    </Link>
                </div>
            </section>
        </>
    )
}