import {motion as m} from "framer-motion"
import Link from "next/link"
import { useEffect, useState } from "react";
import {GrFormCheckmark} from "react-icons/gr"

export default function CongratsPage({prevPage, nextPage}){
  const [loadingDone, setLoadingDone] = useState(false);
  
  
  useEffect(() => {
    let progressValue = 0;
    const interval = setInterval(() => {
      console.log("interval running")
      progressValue++;
      document.querySelector(".progressBar").style.background = `conic-gradient(
        #18732F ${progressValue * 3.6}deg,
        white  ${progressValue * 3.6}deg
    )`
      if(progressValue === 100){
        setLoadingDone(true)
        clearInterval(interval)
      }
    }, 10);
  },[])

  return (
    <>
      <m.div 
      initial={{translateY: "-40rem"}}
      animate={{translateY: "0"}}
      transition={{type: "spring", stiffness: 70  }}
      className="bg-orange-400 w-full h-3/4 rounded-b-[50px] relative">
          <div className="progressBar flex justify-center items-center   absolute top-2/4 left-2/4 -translate-y-2/4 -translate-x-2/4 bg-white h-60 rounded-full aspect-square">
            <div className=" h-[93%] aspect-square bg-orange-400 rounded-full" />
          </div>
          {loadingDone &&  
          <m.div 
          initial={{scale: 0, y: "-50%", x: "-50%"}}
          animate={{scale: 1, y: "-50%", x: "-50%"}}
          transition={{type: "spring"}}
          className="flex justify-center items-center absolute top-2/4 left-2/4  bg-[#34C759] h-60 rounded-full aspect-square">
            <img 
            src="/checkmark.svg" />
          </m.div>}
          {loadingDone &&
          <m.h1 
          initial={{scale: 0}}
          animate={{scale: 1}}
          transition={{type: "spring", delay: .2}}
          className=" font-ubuntu text-white text-2xl absolute bottom-20 text-center w-full">Grattis!<br/> Ditt konto har nu skapats!</m.h1>}
      </m.div>
      <div className="w-full h-1/4 flex justify-center items-end pb-16  ">
       {loadingDone && 
       <Link href={"/welcome"} className="w-full flex justify-center">
        <m.button
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ type: "spring", delay: .4 }}
          className="bg-orange-400 border-2 w-4/5 text-grey-800 font-normal font-ubuntu px-6 py-2 rounded-3xl">
              Fortsätt
          </m.button>
        </Link>}
      </div>
    </>
    )
}