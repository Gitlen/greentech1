import { motion as m } from "framer-motion";
import Link from "next/link";

export default function WelcomePage({ nextPage, goBack }) {
    return (
        <>
            <section className="bg-teal-900 w-screen h-4/5 flex flex-col
            items-center justify-center pb-5 space-y-1">
                <m.h1
                    initial={{ scale: 0 }}
                    animate={{ scale: 1 }}
                    transition={{ type: "spring", delay: .3 }}
                    className="font-ubuntu text-3xl text-white">SOMETHING</m.h1>
            </section>
            <section className="h-full bg-white w-full text-center rounded-t-2xl flex flex-col justify-between relative pb-10 tall:pb-28 pt-10 mx-auto">
                <div className="bg-teal-900 absolute -top-1 left-0 w-full h-24 -z-20" />
                <div className="space-y-6 flex flex-col items-center">
                    <m.h1
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .1 }}
                        className="text-3xl max-w-[17rem] font-ubuntu">Samla pengar till din klassresa</m.h1>
                    <m.p
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .2 }}
                        className="text-md max-w-[14rem] font-ubuntu">
                        Och gör en insats till naturen och stadens invånare på köpet
                    </m.p>
                </div>
                <div className="flex flex-col space-y-6 w-3/4 mx-auto">
                    <Link href={"/register"}>
                    <m.button
                        initial={{ scale: 0 }}
                        animate={{ scale: 1 }}
                        transition={{ type: "spring", delay: .3 }}
                        className="bg-teal-900 w-full text-white font-normal font-ubuntu px-6 py-2 rounded-3xl">
                        Kom igång!
                    </m.button>
                    </Link>
                    <Link href={"/login"}>
                        <m.button
                            initial={{ scale: 0 }}
                            animate={{ scale: 1 }}
                            transition={{ type: "spring", delay: .4 }}
                            className="border-teal-900 border-2 w-full text-teal-900 font-normal font-ubuntu px-6 py-2 rounded-3xl">
                            Logga in
                        </m.button>
                    </Link>
                </div>
            </section>
        </>
    )
}